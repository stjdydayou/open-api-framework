package com.dliyun.oap.docs;

import com.dliyun.oap.framework.annotation.HttpAction;
import lombok.Data;

import java.util.List;

@Data
public class ApiMethod {

    /**
     * API的方法
     */
    private String method;

    /**
     * API的方法的标识
     */
    private String title;

    /**
     * HTTP请求的方法
     */
    private HttpAction[] httpAction;

    /**
     * 对应的版本号，如果为null或""表示不区分版本
     */
    private String version = null;


    /**
     * 请求参数
     */
    private List<ApiMethodField> listFields;
}
