package com.dliyun.oap.docs;

import lombok.Data;

@Data
public class ApiMethodField {
    String name;
    String type;
    String describe;
    boolean required;
}
