package com.dliyun.oap.spring.boot.config;

import com.dliyun.oap.framework.security.AppSecretManager;
import com.dliyun.oap.framework.security.FileUploadController;
import com.dliyun.oap.framework.security.InvokeTimesController;
import com.dliyun.oap.framework.security.ServiceAccessController;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author stjdydayou
 */
@Data
@ConfigurationProperties(prefix = "spring.oap.security")
public class SecurityProperties {
    private Class<? extends FileUploadController> fileUploadControllerClass;
    private Class<? extends AppSecretManager> appSecretManagerClass;
    private Class<? extends ServiceAccessController> serviceAccessControllerClass;
    private Class<? extends InvokeTimesController> invokeTimesControllerClass;
}