package com.dliyun.oap.spring.boot.config;

import com.dliyun.oap.framework.ThreadFerry;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author stjdydayou
 */
@Data
@ConfigurationProperties(prefix = "spring.oap.executor")
public class ExecutorProperties {
	private Class<? extends ThreadFerry> threadFerryClass;
	private Integer corePoolSize = 5;
	private Integer maxPoolSize = 1024;
	private Integer keepAliveSeconds = 120;
	private Integer queueCapacity = 30;
}