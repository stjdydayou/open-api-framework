package com.dliyun.oap.spring.boot.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author stjdydayou
 */
@Data
@ConfigurationProperties(prefix = "spring.oap.upload")
public class UploadProperties {
    private Integer fileMaxSize;
    private List<String> fileTypes;
}