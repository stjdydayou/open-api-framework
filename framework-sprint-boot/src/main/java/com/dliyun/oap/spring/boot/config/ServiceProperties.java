package com.dliyun.oap.spring.boot.config;

import com.dliyun.oap.framework.security.TransportSecurity;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author stjdydayou
 */
@Data
@ConfigurationProperties(prefix = "spring.oap.service")
public class ServiceProperties {
    private Integer timeoutSeconds;
    private String servletPath;
    private Class<? extends TransportSecurity> transportSecurityClass;
}