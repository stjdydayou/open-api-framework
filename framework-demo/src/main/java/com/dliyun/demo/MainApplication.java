package com.dliyun.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author stjdydayou
 */
@Slf4j
@Configuration
@ServletComponentScan(basePackages = "com.dliyun")
@SpringBootApplication(scanBasePackages = {"com.dliyun"})
public class MainApplication {


    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MainApplication.class);
        app.run(args);
    }

}
