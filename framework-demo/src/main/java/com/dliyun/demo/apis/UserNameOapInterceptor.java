package com.dliyun.demo.apis;

import com.alibaba.fastjson.JSON;
import com.dliyun.oap.framework.annotation.ServiceMethod;
import com.dliyun.oap.framework.context.OapRequestContext;
import com.dliyun.oap.framework.interceptor.AbstractOapInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * @author stjdydayou
 */
@Component
@Slf4j
public class UserNameOapInterceptor extends AbstractOapInterceptor {
    @Override
    public void beforeService(OapRequestContext orc, Map<String, Object> requestParams) {
        ServiceMethod serviceMethod = orc.getServiceMethodHandler().getHandlerMethod().getAnnotation(ServiceMethod.class);
        log.info("beforeService ...{}", serviceMethod.method());
    }

    @Override
    public void beforeResponse(OapRequestContext orc, Map<String, Object> requestParams) {
        log.info(JSON.toJSONString(orc.getOapResponse()));
        log.info("beforeResponse ...");
    }
}
