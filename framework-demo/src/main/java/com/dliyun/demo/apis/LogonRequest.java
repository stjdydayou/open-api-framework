/**
 * 版权声明：中图一购网络科技有限公司 版权所有 违者必究 2012
 * 日    期：12-7-14
 */
package com.dliyun.demo.apis;

import com.dliyun.oap.framework.annotation.ServiceParamField;
import com.dliyun.oap.framework.request.AbstractOapRequest;
import com.dliyun.oap.framework.request.UploadFile;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;


/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class LogonRequest extends AbstractOapRequest {

	@ServiceParamField(describe = "用户名")
	@NotNull
	@Pattern(regexp = "\\w{4,30}")
	private String userName;

	@ServiceParamField(describe = "密码")
	@NotBlank
	@Pattern(regexp = "\\w{6,30}")
	private String password;

	private List<String> ss;

	@ServiceParamField(describe = "上传文件")
	private UploadFile file;
}

