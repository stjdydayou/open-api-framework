package com.dliyun.demo.apis;

import com.dliyun.oap.framework.annotation.ServiceMethod;
import com.dliyun.oap.framework.annotation.ServiceMethodBean;
import com.dliyun.oap.framework.response.OapResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
@Slf4j
@ServiceMethodBean
public class UserApi {

    @Autowired
    private ObjectMapper objectMapper;

    @ServiceMethod(method = "user.login", title = "用户登录", groupTitle = "")
    public OapResponse login(LogonRequest request) {

        Map<String, Object> m = new HashMap<>();
        m.put("userName", request.getUserName());
        m.put("password", request.getPassword());
        m.put("ss", request.getSs());

        return OapResponse.success().setBody(m);
    }
}
