package com.dliyun.oap.framework.annotation;


import lombok.Data;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
@Data
public class ServiceMethodDefinition {

	/**
	 * 默认的组
	 */
	public static final String DEFAULT_GROUP = "DEFAULT";

	/**
	 * 默认分组标识
	 */
	public static final String DEFAULT_GROUP_TITLE = "DEFAULT GROUP";

	/**
	 * API的方法
	 */
	private String method;

	/**
	 * API的方法的标识
	 */
	private String methodTitle;

	/**
	 * HTTP请求的方法
	 */
	private HttpAction[] httpAction;

	/**
	 * API方法所属组名
	 */
	private String methodGroup = DEFAULT_GROUP;

	/**
	 * API方法组名的标识
	 */
	private String methodGroupTitle;

	/**
	 * API所属的标签
	 */
	private String[] tags = {};

	/**
	 * 过期时间，单位为秒，0或负数表示不过期
	 */
	private int timeout = -9999;

	/**
	 * 对应的版本号，如果为null或""表示不区分版本
	 */
	private String version = null;

	/**
	 * 服务方法是否已经过期
	 */
	private boolean obsoleted = false;
}

