package com.dliyun.oap.framework.request;

import com.dliyun.oap.framework.context.OapRequestContext;

/**
 * <pre>
 *    OAP服务的请求对象，请求方法的入参必须是继承于该类。
 * </pre>
 *
 * @version 1.0
 */
public interface OapRequest {

    /**
     * 获取服务方法的上下文
     *
     * @return
     */
    OapRequestContext getRequestContext();

}

