package com.dliyun.oap.framework.security;

import com.dliyun.oap.framework.exception.TransportException;

/**
 * 针对网络传输过程的安全，对接收参数与响应参数进行加密与解密服务，默认情况下进行明文传输
 */
public interface TransportSecurity {

	/**
	 * 对应答内容进行加密处理
	 *
	 * @param s
	 * @return
	 */
	String responseEncrypt(String appKey, String s) throws TransportException;

	/**
	 * 对接收的数据进行解密处理
	 *
	 * @param s
	 * @return
	 */
	String requestDecrypt(String appKey, String s) throws TransportException;
}
