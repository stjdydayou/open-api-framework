/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-6-1
 */
package com.dliyun.oap.framework.context;

import com.dliyun.oap.framework.request.OapRequest;
import org.springframework.context.ApplicationContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * <pre>
 *    更改请求对象创建{@link OapRequestContext}实例,子类可以根据多种传输协议定义自己的创建器。
 * </pre>
 *
 * @version 1.0
 */
public interface RequestContextBuilder {
	/**
	 * 根据reqeuest及response请求响应对象，创建{@link OapRequestContext}实例。绑定系统参数，请求对象
	 *
	 * @param oapContext
	 * @param request
	 * @param response
	 * @return
	 */
	OapRequestContext buildBySysParams(OapContext oapContext,
	                                   HttpServletRequest request,
	                                   HttpServletResponse response,
	                                   Map<String, Object> requestParams,
	                                   String appKey);

	/**
	 * 绑定业务参数
	 *
	 * @param requestContext
	 */
	OapRequest buildOapRequest(OapRequestContext requestContext, Map<String, Object> requestParams);

	/**
	 * 初始化类型转换器
	 *
	 * @param applicationContext
	 */
	void registerConverters(ApplicationContext applicationContext);
}

