package com.dliyun.oap.framework.response;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public class ServiceUnavailableErrorResponse extends OapResponse {

	// 注意，这个不能删除，否则无法进行流化
	public ServiceUnavailableErrorResponse() {
	}

	public ServiceUnavailableErrorResponse(Throwable throwable) {
		this.setCode("SERVICE_UNAVAILABLE").setMsg(String.format("调用后端服务异常。异常信息:%s", throwable.toString()));
	}
}
