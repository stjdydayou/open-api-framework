package com.dliyun.oap.framework;

/**
 * @author stjdydayou
 */
public class SystemParameterNames {

    // 方法的默认参数名
    public static final String METHOD = "method";

    // 服务版本号的默认参数名
    public static final String VERSION = "version";
}
