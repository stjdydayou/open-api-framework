package com.dliyun.oap.framework.context;

import com.dliyun.oap.framework.annotation.HttpAction;
import com.dliyun.oap.framework.response.OapResponse;
import com.dliyun.oap.framework.service.ServiceMethodHandler;
import lombok.Data;
import org.springframework.validation.ObjectError;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;

/**
 * @author stjdydayou
 */
@Data
public class OapRequestContext {

    private String method;

    private String version;

    private String appKey;

    private ServiceMethodHandler serviceMethodHandler;

    private OapResponse oapResponse;

    private long serviceBeginTime;

    private long serviceEndTime = -1;

    private String clientIp;

    private HttpAction httpAction;

    private HttpServletRequest request;

    private HttpServletResponse response;

    private final String requestId;

    private List<ObjectError> bindingErrors;

    public OapRequestContext() {
        String uuid = UUID.randomUUID().toString();
        this.serviceBeginTime = System.currentTimeMillis();
        this.requestId = uuid.replace("-", "");
    }
}
