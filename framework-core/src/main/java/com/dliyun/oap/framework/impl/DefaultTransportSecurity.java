package com.dliyun.oap.framework.impl;

import com.dliyun.oap.framework.exception.TransportException;
import com.dliyun.oap.framework.security.TransportSecurity;
import com.dliyun.oap.framework.security.algorithm.AES;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class DefaultTransportSecurity implements TransportSecurity {

    @Override
    public String responseEncrypt(String appKey, String s) throws TransportException {
        if (StringUtils.isNotBlank(appKey)) {
            String secret = FileBaseAppSecretManager.getSecret(appKey);
            s = AES.encryptToBase64(s, secret);
        }
        return s;
    }

    @Override
    public String requestDecrypt(String appKey, String s) throws TransportException {
        if (StringUtils.isNotBlank(appKey)) {
            String secret = FileBaseAppSecretManager.getSecret(appKey);
            s = AES.decryptFromBase64(s, secret);
        }
        return s;
    }
}
