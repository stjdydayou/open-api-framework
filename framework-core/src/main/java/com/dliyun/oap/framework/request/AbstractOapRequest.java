/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-6-6
 */
package com.dliyun.oap.framework.request;

import com.dliyun.oap.framework.context.OapRequestContext;
import com.dliyun.oap.framework.annotation.Temporary;

/**
 * <pre>
 *   所有请求对象应该通过扩展此抽象类实现
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public abstract class AbstractOapRequest implements OapRequest {

    @Temporary
    private OapRequestContext requestContext;


    @Override
    public OapRequestContext getRequestContext() {
        return requestContext;
    }

    public final void setRequestContext(OapRequestContext requestContext) {
        this.requestContext = requestContext;
    }
}

