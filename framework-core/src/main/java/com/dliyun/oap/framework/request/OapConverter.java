package com.dliyun.oap.framework.request;

import org.springframework.core.convert.converter.Converter;

/**
 * <pre>
 * 功能说明：
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
public interface OapConverter<S, T> extends Converter<S, T> {

    /**
     * 从T转换成S
     *
     * @param target
     * @return
     */
    S unconvert(T target);

    /**
     * 获取源类型
     *
     * @return
     */
    Class<S> getSourceClass();

    /**
     * 获取目标类型
     *
     * @return
     */
    Class<T> getTargetClass();
}

