/**
 * 日    期：12-2-27
 */
package com.dliyun.oap.framework.response;

import com.dliyun.oap.framework.security.TransportSecurity;

import java.io.IOException;
import java.io.OutputStream;

/**
 * <pre>
 *   负责将请求方法返回的响应对应流化为相应格式的内容。
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public interface Marshaller {
    /**
     * 返回数据和格式化
     *
     * @param appKey
     * @param object
     * @param outputStream
     * @param transportSecurity
     * @throws IOException
     */
    void marshaller(String appKey, OapResponse object, OutputStream outputStream, TransportSecurity transportSecurity) throws Exception;
}

