package com.dliyun.oap.framework.impl;

import com.dliyun.oap.framework.security.InvokeTimesController;

/**
 * <pre>
 *    默认的实现
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public class DefaultInvokeTimesController implements InvokeTimesController {

    @Override
    public boolean isAppInvokeLimitExceed(String appKey) {
        return false;
    }


    @Override
    public boolean isAppInvokeFrequencyExceed(String appKey) {
        return false;
    }
}

