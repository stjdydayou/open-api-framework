package com.dliyun.oap.framework.impl;

import com.dliyun.oap.framework.security.ServiceAccessController;

/**
 * <pre>
 * 功能说明：对调用的方法进行安全性检查
 * </pre>
 *
 * @version 1.0
 */
public class DefaultServiceAccessController implements ServiceAccessController {


    @Override
    public boolean isAppGranted(String appKey, String method, String version) {
        return true;
    }

}

