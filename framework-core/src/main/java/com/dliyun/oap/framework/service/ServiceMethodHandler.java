package com.dliyun.oap.framework.service;

import com.dliyun.oap.framework.annotation.ServiceMethodDefinition;
import com.dliyun.oap.framework.request.OapRequest;

import java.lang.reflect.Method;
import java.util.List;

/**
 * <pre>
 *     服务处理器的相关信息
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public class ServiceMethodHandler {

    //处理器对象
    private Object handler;

    //处理器的处理方法
    private Method handlerMethod;

    private ServiceMethodDefinition serviceMethodDefinition;

    //处理方法的请求对象类
    private Class<? extends OapRequest> requestType = OapRequest.class;

    //属性类型为FileItem的字段列表
    private List<String> uploadFileFieldNames;

    //是否是实现类
    private boolean requestImplType;

    public ServiceMethodHandler() {
    }

    public Object getHandler() {
        return handler;
    }

    public void setHandler(Object handler) {
        this.handler = handler;
    }

    public Method getHandlerMethod() {
        return handlerMethod;
    }

    public void setHandlerMethod(Method handlerMethod) {
        this.handlerMethod = handlerMethod;
    }

    public Class<? extends OapRequest> getRequestType() {
        return requestType;
    }

    public void setRequestType(Class<? extends OapRequest> requestType) {
        this.requestType = requestType;
    }

    public boolean isHandlerMethodWithParameter() {
        return this.requestType != null;
    }

    public ServiceMethodDefinition getServiceMethodDefinition() {
        return serviceMethodDefinition;
    }

    public void setServiceMethodDefinition(ServiceMethodDefinition serviceMethodDefinition) {
        this.serviceMethodDefinition = serviceMethodDefinition;
    }

    public static String methodWithVersion(String methodName, String version) {
        return methodName + "#" + version;
    }

    public boolean isRequestImplType() {
        return requestImplType;
    }

    public void setRequestImplType(boolean requestImplType) {
        this.requestImplType = requestImplType;
    }

    public List<String> getUploadFileFieldNames() {
        return uploadFileFieldNames;
    }

    public void setUploadFileFieldNames(List<String> uploadFileFieldNames) {
        this.uploadFileFieldNames = uploadFileFieldNames;
    }

    public boolean hasUploadFiles() {
        return uploadFileFieldNames != null && uploadFileFieldNames.size() > 0;
    }
}

