package com.dliyun.oap.framework.service;

import com.dliyun.oap.framework.ThreadFerry;
import com.dliyun.oap.framework.context.OapContext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * OAP的服务路由器，服务方法必须位于@Controller的类中，服务方法使用{@link com.dliyun.oap.framework.annotation.ServiceMethod}注解
 *
 * @author stjdydayou
 * @version 1.0
 */
public interface RouterService {
	/**
	 * OAP框架的总入口，一般框架实现，开发者无需关注。
	 *
	 * @param request
	 * @param response
	 */
	void service(HttpServletRequest request, HttpServletResponse response);


	/**
	 * 设置执行线程
	 *
	 * @param threadPoolExecutor
	 */
	void setThreadPoolExecutor(ThreadPoolExecutor threadPoolExecutor);

	/**
	 * 设置线程信息摆渡器
	 *
	 * @param threadFerryClass
	 */
	void setThreadFerryClass(Class<? extends ThreadFerry> threadFerryClass);

	/**
	 * 设置所有服务的通用过期时间，单位为秒
	 *
	 * @param timeoutSeconds
	 */

	void setTimeoutSeconds(Integer timeoutSeconds);

	/**
	 * 获取框架上下文
	 *
	 * @return
	 */
	OapContext getOapContext();
}

