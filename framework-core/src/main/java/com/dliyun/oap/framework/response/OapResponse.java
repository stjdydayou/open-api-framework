package com.dliyun.oap.framework.response;

import lombok.NonNull;

/**
 * @author stjdydayou
 */
public class OapResponse {

    private String code = "SUCCESS";

    private String msg = "请求成功";

    private Object body;

    public String getCode() {
        return code;
    }

    public OapResponse setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public OapResponse setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getBody() {
        return body;
    }

    public OapResponse setBody(Object body) {
        this.body = body;
        return this;
    }


    public static OapResponse success() {
        OapResponse response = new OapResponse();
        response.setCode("SUCCESS");
        return response;
    }


    public static OapResponse fail(String code, @NonNull String msg) {
        OapResponse response = new OapResponse();
        response.setCode(code).setMsg(msg);
        return response;
    }
}
