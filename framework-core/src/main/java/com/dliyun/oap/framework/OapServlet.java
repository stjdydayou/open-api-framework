package com.dliyun.oap.framework;

import com.dliyun.oap.framework.service.RouterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author stjdydayou
 */
@Slf4j
public class OapServlet extends HttpServlet {

    private RouterService routerService;

    /**
     * 将请求导向到Oap的框架中。
     *
     * @param req
     * @param resp
     */

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        routerService.service(req, resp);
    }

    @Override
    public void init(ServletConfig config) {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext ctx = (ApplicationContext) servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
        this.routerService = ctx.getBean(RouterService.class);
    }
}
