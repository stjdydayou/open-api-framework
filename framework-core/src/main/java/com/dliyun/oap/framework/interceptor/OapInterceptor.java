/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-4-25
 */
package com.dliyun.oap.framework.interceptor;

import com.dliyun.oap.framework.context.OapRequestContext;
import com.dliyun.oap.framework.response.OapResponse;

import java.util.Map;

/**
 * <pre>
 *   拦截器，将在服务之前，服务之后响应之前调用
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
public interface OapInterceptor {

    /**
     * 在进行服务之前调用,如果在方法中往{@link OapRequestContext}设置了{@link OapResponse}（相当于已经产生了响应了）,
     * 所以服务将直接返回，不往下继续执行，反之服务会继续往下执行直到返回响应
     *
     * @param orc
     */
    void beforeService(OapRequestContext orc, Map<String, Object> requestParams);

    /**
     * 在服务之后，响应之前调用
     *
     * @param orc
     */
    void beforeResponse(OapRequestContext orc, Map<String, Object> requestParams);

    /**
     * 该方法返回true时才实施拦截，否则不拦截。可以通过{@link OapRequestContext}
     *
     * @param orc
     * @return
     */
    boolean isMatch(OapRequestContext orc);

    /**
     * 执行的顺序，整数值越小的越早执行
     *
     * @return
     */
    int getOrder();
}

