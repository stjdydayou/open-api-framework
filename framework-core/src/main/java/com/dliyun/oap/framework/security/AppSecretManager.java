package com.dliyun.oap.framework.security;

/**
 * <pre>
 *    应用键管理器，可根据appKey获取对应的secret.
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public interface AppSecretManager {
    /**
     * 是否是合法的appKey
     *
     * @param appKey
     * @return
     */
    boolean isValidAppKey(String appKey);
}

