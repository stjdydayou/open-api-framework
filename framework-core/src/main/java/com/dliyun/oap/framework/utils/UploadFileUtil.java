package com.dliyun.oap.framework.utils;


import com.dliyun.oap.framework.exception.IllegalUploadFileFormatException;
import com.dliyun.oap.framework.request.UploadFile;

import java.util.Base64;

/**
 * <pre>
 *     Oap的上传文件编码格式为：
 *   fileType@BASE64编码的文件内容
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
public class UploadFileUtil {

	public static final char SPERATOR = '@';

	/**
	 * 获取文件的类型
	 *
	 * @param encodeFile
	 * @return
	 */
	public static String getFileType(String encodeFile) {
		int speratorIndex = encodeFile.indexOf(SPERATOR);
		if (speratorIndex > -1) {
			String fileType = encodeFile.substring(0, speratorIndex);
			return fileType.toLowerCase();
		} else {
			throw new IllegalUploadFileFormatException("文件格式不对，正确格式为：<文件格式>@<文件内容>");
		}
	}

	/**
	 * 获取文件的字节数组
	 *
	 * @param encodeFile
	 * @return
	 */
	public static byte[] decode(String encodeFile) {
		int speratorIndex = encodeFile.indexOf(SPERATOR);
		if (speratorIndex > -1) {
			String content = encodeFile.substring(speratorIndex + 1);
			return Base64.getDecoder().decode(content);
		} else {
			throw new IllegalUploadFileFormatException("文件格式不对，正确格式为：<文件格式>@<文件内容>");
		}
	}

	/**
	 * 将文件编码为BASE64的字符串
	 *
	 * @param bytes
	 * @return
	 */
	public static String encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}

	/**
	 * 将文件编码为一个字符串
	 *
	 * @param uploadFile
	 * @return
	 */
	public static String encode(UploadFile uploadFile) {
		return uploadFile.getFileType() + SPERATOR + encode(uploadFile.getContent());
	}
}

