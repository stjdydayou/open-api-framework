package com.dliyun.oap.framework.interceptor;

import com.dliyun.oap.framework.context.OapRequestContext;
import lombok.extern.slf4j.Slf4j;

/**
 * <pre>
 *    抽象拦截器，实现类仅需覆盖特定的方法即可。
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
@Slf4j
public abstract class AbstractOapInterceptor implements OapInterceptor {

	@Override
	public boolean isMatch(OapRequestContext orc) {
		return true;
	}

	/**
	 * 放在拦截器链的最后
	 *
	 * @return
	 */
	@Override
	public int getOrder() {
		return 0;
	}
}

