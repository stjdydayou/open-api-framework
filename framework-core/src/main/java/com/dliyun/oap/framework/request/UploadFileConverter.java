/**
 * 版权声明： 版权所有 违者必究 2012
 * 日    期：12-8-1
 */
package com.dliyun.oap.framework.request;

import com.dliyun.oap.framework.utils.UploadFileUtil;
import org.springframework.stereotype.Component;

/**
 * <pre>
 *   将以BASE64位编码字符串转换为字节数组的{@link UploadFile}对象
 * </pre>
 *
 * @author 陈雄华
 * @version 1.0
 */
@Component
public class UploadFileConverter implements OapConverter<String, UploadFile> {


    @Override
    public UploadFile convert(String source) {
        String fileType = UploadFileUtil.getFileType(source);
        byte[] contentBytes = UploadFileUtil.decode(source);
        return new UploadFile(fileType, contentBytes);
    }


    @Override
    public String unconvert(UploadFile target) {
        return UploadFileUtil.encode(target);
    }


    @Override
    public Class<String> getSourceClass() {
        return String.class;
    }


    @Override
    public Class<UploadFile> getTargetClass() {
        return UploadFile.class;
    }
}

