package com.dliyun.oap.framework.annotation;


import java.lang.annotation.*;

/**
 * <pre>
 *     使用该注解对请参数进行说明
 *
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ServiceParamField {

    /**
     * 服务的方法名，即由method参数指定的服务方法名
     *
     * @return
     */
    String describe();
}
