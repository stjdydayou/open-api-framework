package com.dliyun.oap.framework.exception;

public class TransportException extends Exception {

    private static final long serialVersionUID = 5988622523088194044L;

    public TransportException() {
    }

    public TransportException(String message) {
        super(message);
    }

    public TransportException(String message, Throwable cause) {
        super(message, cause);
    }

    public TransportException(Throwable cause) {
        super(cause);
    }
}
