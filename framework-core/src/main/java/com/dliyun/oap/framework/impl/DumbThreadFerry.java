package com.dliyun.oap.framework.impl;

import com.dliyun.oap.framework.ThreadFerry;

/**
 * @author stjdydayou
 */
public class DumbThreadFerry implements ThreadFerry {

    @Override
    public void doInSrcThread() {
    }


    @Override
    public void doInDestThread() {
    }
}
