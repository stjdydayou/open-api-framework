package com.dliyun.oap.framework.security;

import com.dliyun.oap.framework.context.OapContext;
import com.dliyun.oap.framework.context.OapRequestContext;
import com.dliyun.oap.framework.response.OapResponse;

import java.util.Map;

/**
 * <pre>
 *   负责对请求数据、会话、业务安全、应用密钥安全进行检查并返回相应的错误
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public interface SecurityManager {

    /**
     * 对请求服务的上下文进行检查校验
     *
     * @param requestContext
     * @return
     */
    OapResponse validateSystemParameters(OapRequestContext requestContext, OapContext oapContext);

    /**
     * 验证其它的事项：包括业务参数，业务安全性，会话安全等
     *
     * @param requestContext
     * @return
     */
    OapResponse validateOther(OapRequestContext requestContext, Map<String, Object> requestParams);
}
