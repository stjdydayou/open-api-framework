package com.dliyun.oap.framework.security;

/**
 * <pre>
 *    安全控制控制器，决定用户是否有。
 * </pre>
 *
 * @author stjdydayou
 * @version 1.0
 */
public interface ServiceAccessController {

    /**
     * 服务方法是否向ISV开放
     *
     * @param appKey
     * @param method
     * @param version
     * @return
     */
    boolean isAppGranted(String appKey, String method, String version);

}

