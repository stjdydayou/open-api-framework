package com.dliyun.oap.framework.response;

import com.alibaba.fastjson.JSON;
import com.dliyun.oap.framework.security.TransportSecurity;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author stjdydayou
 */
public class FastJsonMarshaller implements Marshaller {
    @Override
    public void marshaller(String appKey, OapResponse object, OutputStream outputStream, TransportSecurity transportSecurity) throws Exception {
        if (object.getBody() != null) {
            Object body = object.getBody();
            String jsonBody = JSON.toJSONString(body);
            String encryptBody = transportSecurity.responseEncrypt(appKey, jsonBody);
            object.setBody(encryptBody);
        }
        String resultJson = JSON.toJSONString(object);
        outputStream.write(resultJson.getBytes(StandardCharsets.UTF_8));
    }
}
