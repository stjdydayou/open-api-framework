package com.dliyun.oap.framework.exception;

public class OapException extends RuntimeException {
    private static final long serialVersionUID = 5988622523088194044L;

    public OapException() {
    }

    public OapException(String message) {
        super(message);
    }

    public OapException(String message, Throwable cause) {
        super(message, cause);
    }

    public OapException(Throwable cause) {
        super(cause);
    }
}
