package com.dliyun.oap.framework.security;

/**
 * <pre>
 *   服务访问次数及频率的控制管理器
 * </pre>
 *
 * @version 1.0
 */
public interface InvokeTimesController {

    /**
     * 应用的服务访问次数是否超限
     *
     * @param appKey
     * @return
     */
    boolean isAppInvokeLimitExceed(String appKey);

    /**
     * 应用对服务的访问频率是否超限
     *
     * @param appKey
     * @return
     */
    boolean isAppInvokeFrequencyExceed(String appKey);
}

