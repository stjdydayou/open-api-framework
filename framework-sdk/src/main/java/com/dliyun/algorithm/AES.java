package com.dliyun.algorithm;


import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.util.Base64;


public class AES {

    public static final String AES_ALGORITHM = "AES/ECB/PKCS5Padding";

    public static final String CHAR_ENCODING = "UTF-8";

    /**
     * 加密
     *
     * @param data 需要加密的内容
     * @param key  加密密码
     * @return
     */
    private static byte[] encrypt(byte[] data, byte[] key) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec seckey = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGORITHM);// 创建密码器
            cipher.init(Cipher.ENCRYPT_MODE, seckey);// 初始化
            return cipher.doFinal(data); // 加密
        } catch (Exception e) {
            throw new RuntimeException("encrypt fail!", e);
        }
    }

    /**
     * 解密
     *
     * @param data 待解密内容
     * @param key  解密密钥
     * @return
     */
    private static byte[] decrypt(byte[] key, byte[] data) {
        try {
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");
            byte[] enCodeFormat = secretKey.getEncoded();
            SecretKeySpec seckey = new SecretKeySpec(enCodeFormat, "AES");
            Cipher cipher = Cipher.getInstance(AES_ALGORITHM);// 创建密码器
            cipher.init(Cipher.DECRYPT_MODE, seckey);// 初始化
            return cipher.doFinal(data); // 加密
        } catch (Exception e) {
            throw new RuntimeException("decrypt fail!", e);
        }
    }

    public static String encryptToBase64(String data, String key) {
        try {
            byte[] valueByte = encrypt(data.getBytes(CHAR_ENCODING), key.getBytes(CHAR_ENCODING));
            return new String(Base64.getEncoder().encode(valueByte));
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("encrypt fail!", e);
        }

    }

    public static String decryptFromBase64(String data, String key) {
        try {
            byte[] originalData = Base64.getDecoder().decode(data.getBytes());
            byte[] valueByte = decrypt(originalData, key.getBytes(CHAR_ENCODING));
            return new String(valueByte, CHAR_ENCODING);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("decrypt fail!", e);
        }
    }

//    public static String encryptWithBase64Key(String key, String data) {
//        try {
//            byte[] valueByte = encrypt(Base64.getDecoder().decode(key.getBytes()), data.getBytes(CHAR_ENCODING));
//            return new String(Base64.getEncoder().encode(valueByte));
//        } catch (UnsupportedEncodingException e) {
//            throw new RuntimeException("encrypt fail!", e);
//        }
//    }
//
//    public static String decryptWithBase64Key(String key, String data) {
//        try {
//            byte[] originalData = Base64.getDecoder().decode(data.getBytes());
//            byte[] valueByte = decrypt(Base64.getDecoder().decode(key.getBytes()), originalData);
//            return new String(valueByte, CHAR_ENCODING);
//        } catch (UnsupportedEncodingException e) {
//            throw new RuntimeException("decrypt fail!", e);
//        }
//    }
//
//    public static byte[] genarateRandomKey() {
//        KeyGenerator keygen = null;
//        try {
//            keygen = KeyGenerator.getInstance(AES_ALGORITHM);
//        } catch (NoSuchAlgorithmException e) {
//            throw new RuntimeException(" genarateRandomKey fail!", e);
//        }
//        SecureRandom random = new SecureRandom();
//        keygen.init(random);
//        Key key = keygen.generateKey();
//        return key.getEncoded();
//    }
//
//    public static String genarateRandomKeyWithBase64() {
//        return new String(Base64.getEncoder().encode(genarateRandomKey()));
//    }

}
