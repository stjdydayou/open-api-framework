package com.dliyun;

public class Response<T> {

    private String code;

    private String msg;

    private T body;

    public boolean isSuccessful() {
        return "SUCCESS".equals(this.code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }
}
