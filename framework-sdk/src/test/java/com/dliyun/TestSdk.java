package com.dliyun;

import com.alibaba.fastjson.JSON;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TestSdk {


	public static void main(String[] args) throws Exception {
		DefaultClient client = new DefaultClient(
				"http://127.0.0.1:9800/router",
				"220228000055",
				"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCMbziui2H+WBpd0TbA7tNrqnk5X0ecoqD5PZjnRscwQ88Q0STqyOe2PE6LjFPG9mQOOhKAJqbZRPeyzXuLIGlMrUZ2BmAl488n5mxkO24DPnC42/1OTHWjOjRHndPA/IyuEzv7lVkhMTj9hkoxz45yP3s/O/kUSgYH9FkF/wl2MQIDAQAB",
				AlgorithmType.RSA
		);
		Map<String, Object> form1 = new HashMap<>();
		form1.put("current", "1");
		form1.put("pageSize", "10");
		form1.put("popularFeelingsId", "220228000074");
		Response<Map<String, Object>> response = client.execute("popular.feelings.findDetailPage", "1.0.0", form1);
		System.out.println(JSON.toJSONString(response));
	}
}
